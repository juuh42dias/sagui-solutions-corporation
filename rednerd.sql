-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 31/05/2012 às 01h18min
-- Versão do Servidor: 5.5.16
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `rednerd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `obs` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `administrador`
--

INSERT INTO `administrador` (`id`, `nome`, `email`, `senha`, `obs`) VALUES
(3, 'Raphael Rezende de Oliveira', 'raphaelnaweb@hotmail.com', '1234', 'wwww');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`) VALUES
(2, 'Teste'),
(5, 'Linux');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_post` int(11) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `comentario` varchar(240) DEFAULT NULL,
  `data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`id`, `id_post`, `nome`, `comentario`, `data`) VALUES
(1, 46, 'Raphael', 'O windows manda', 'Quarta-Feira, 09 de Maio de 2012, Ã s 14:09'),
(4, 43, 'Raphael Rezende de Oliveira', 'Eu quero um :]', 'Quarta-Feira, 09 de Maio de 2012, Ã s 14:32'),
(5, 46, 'Tim Cook', 'q bosta', 'Quinta-Feira, 10 de Maio de 2012, Ã s 17:56'),
(6, 47, 'Raphael Rezende de Oliveira', 'boco', 'Quinta-Feira, 10 de Maio de 2012, Ã s 20:11'),
(7, 39, 'jose', 'Reno bocÃ³', 'Segunda-Feira, 21 de Maio de 2012, Ã s 22:04'),
(8, 43, 'jose', '', 'Quarta-Feira, 23 de Maio de 2012, Ã s 20:19'),
(9, 43, 'jose', '', 'Quarta-Feira, 23 de Maio de 2012, Ã s 20:21'),
(11, 42, '', '', 'Domingo, 27 de Maio de 2012, Ã s 12:33'),
(12, 43, 'jose', 'gggggggg', 'Segunda-Feira, 28 de Maio de 2012, Ã s 20:09'),
(13, 5, 'gfdcgfg', 'k,kÃ§l vnbnknhjvnbvc', 'TerÃ§a-Feira, 29 de Maio de 2012, Ã s 17:54'),
(14, 5, 'zeh', 'serio', 'TerÃ§a-Feira, 29 de Maio de 2012, Ã s 17:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `envioPost`
--

CREATE TABLE IF NOT EXISTS `envioPost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `titulo` varchar(75) DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `data` varchar(45) DEFAULT NULL,
  `video` varchar(45) DEFAULT NULL,
  `imagem` varchar(200) DEFAULT NULL,
  `texto` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Extraindo dados da tabela `envioPost`
--

INSERT INTO `envioPost` (`id`, `nome`, `email`, `titulo`, `categoria`, `data`, `video`, `imagem`, `texto`) VALUES
(29, 'raphael', 'raphaelnaweb@hotmail.com', 'Teste imagem', 'cbcbcb', 'Domingo, 27 de Maio de 2012, Ã s 18:37', 'http://www.youtube.com/watch?v=Zcz3t5Wj9Ag', 'seta.png', 'lllll');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`) VALUES
(1, 'raphaelnaweb@hotmail.com'),
(2, 'raphaelnaweb@hotmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE IF NOT EXISTS `parceiros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `responsavel` varchar(100) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `nome`, `responsavel`, `email`, `link`, `tipo`) VALUES
(10, 'Facebook', 'Mark Zuckerberg', 'mark@gmail.com', 'http://www.facebook.com.br', 'Rede Social'),
(11, 'Google', 'ZÃ© da Silva', 'zedasilva@hotmail.com', 'http://www.google.com.br', 'Buscas'),
(12, 'Pousada', 'ZÃ© NinguÃ©m', 'zenimgume@hotmail.com', 'http://www.rioquenterisort.com.br', 'Resort');

-- --------------------------------------------------------

--
-- Estrutura da tabela `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` varchar(100) DEFAULT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `data` varchar(45) DEFAULT NULL,
  `post` varchar(10000) DEFAULT NULL,
  `imagem` varchar(200) DEFAULT NULL,
  `video` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Extraindo dados da tabela `post`
--

INSERT INTO `post` (`id`, `autor`, `titulo`, `categoria`, `data`, `post`, `imagem`, `video`) VALUES
(5, 'raphael', 'Acessibilidade na redaÃ§Ã£o e pelo direito de', 'nova', 'Sabado, 05 de Maio de 2012, Ã s 13:54', 'Ã¡ pensou em como o mundo seria melhor e mais justo se a comunicaÃ§Ã£o de tudo que Ã© do interesse pÃºblico fosse escrito de forma clara e simples, de modo que todos pudessem entender? No vÃ­deo a seguir, a portuguesa Sandra Fisher-Martins no TED Porto mostra de forma divertida e curiosa como os documentos do dia a dia como bulas de remÃ©dio e contratos de locaÃ§Ã£o de imÃ³vel, sÃ£o difÃ­ceis de serem compreendidos pela grande maioria da populaÃ§Ã£o e como isso Ã© um tipo de â€œbarreiraâ€ social que precisa ser quebrada.', NULL, NULL),
(27, 'raphael', 'Acessibilidade na redaÃ§Ã£o e pelo direito de entender', 'nova', 'Sabado, 05 de Maio de 2012, Ã s 16:45', 'ConheÃ§o este livro desde 2007, quando fui sorteado no EBAI â€” Encontro Brasileiro de Arquitetura da InformaÃ§Ã£o â€” com a primeira ediÃ§Ã£o original em inglÃªs da Rockport americana. No inÃ­cio de 2010 fiquei sabendo que os autores haviam lanÃ§ado uma ediÃ§Ã£o revista e atualizada, e com 25 princÃ­pios a mais que a ediÃ§Ã£o anterior. Oras, isso seria 25% a mais de princÃ­pios de design : ) Meses atrÃ¡s a Editora Bookman publicou essa mesma cobiÃ§adissima segunda ediÃ§Ã£o ampliada e revisada, em portuguÃªs. PrincÃ­pios Universais do Design â€” 125 maneiras de aprimorar a usabilidade, influenciar a percepÃ§Ã£o, aumentar o apelo e ensinar por meio do design (Autores: William Lidwell, Kristina Holden & Jill Butler â€” 272 pÃ¡ginas â€” Formato: 22Ã—26 â€” ISBN:9788577807383 â€” Ano: 2011) da Editora Bookman. ', '8452df2c902261073a842835cbc4ca723d31bd22-his.jpg', '6tEihN5DTCE'),
(28, 'Raphael Rezende de Oliveira', 'Acessibilidade na redaÃ§Ã£o e pelo direito de entender', 'Teste', 'Sun, 06 de Maio de 2012, Ã s 19:48', 'ConheÃ§o este livro desde 2007, quando fui sorteado no EBAI â€” Encontro Brasileiro de Arquitetura da InformaÃ§Ã£o â€” com a primeira ediÃ§Ã£o original em inglÃªs da Rockport americana. \r\nNo inÃ­cio de 2010 fiquei sabendo que os autores haviam lanÃ§ado uma ediÃ§Ã£o revista e atualizada, e com 25 princÃ­pios a mais que a ediÃ§Ã£o anterior. Oras, isso seria 25% a mais de princÃ­pios de design : ) Meses atrÃ¡s a Editora Bookman publicou essa mesma cobiÃ§adissima segunda ediÃ§Ã£o ampliada e revisada, em portuguÃªs. \r\nPrincÃ­pios Universais do Design â€” 125 maneiras de aprimorar a usabilidade, influenciar a percepÃ§Ã£o, aumentar o apelo e ensinar por meio do design (Autores: William Lidwell, Kristina Holden & Jill Butler â€” 272 pÃ¡ginas â€” Formato: 22Ã—26 â€” ISBN:9788577807383 â€” Ano: 2011) da Editora Bookman. ', 'da405a634b6e23353a86a1acdff0a847b16770ca-his.jpg', ''),
(29, 'Raphael Rezende de Oliveira', 'Teste de tags', 'Teste', 'Domingo, 06 de Maio de 2012, Ã s 20:17', 'ConheÃ§o este livro desde 2007, quando fui sorteado no EBAI â€” <i>Encontro Brasileiro de Arquitetura da InformaÃ§Ã£o </i>â€” com a primeira ediÃ§Ã£o original em inglÃªs da Rockport americana. No inÃ­cio de 2010 fiquei sabendo que os autores haviam lanÃ§ado uma ediÃ§Ã£o revista e atualizada, e com 25 princÃ­pios a mais que a ediÃ§Ã£o anterior. \r\n<p>Oras, isso seria 25% a mais de princÃ­pios de design : ) Meses atrÃ¡s a Editora Bookman publicou essa mesma cobiÃ§adissima segunda ediÃ§Ã£o ampliada e revisada, em portuguÃªs.</p>\r\n<p><b> PrincÃ­pios Universais do Design</b> â€” 125 maneiras de aprimorar a usabilidade, influenciar a percepÃ§Ã£o, aumentar o apelo e ensinar por meio do design (Autores: William Lidwell, Kristina Holden & Jill Butler â€” 272 pÃ¡ginas â€” Formato: 22Ã—26 â€” ISBN:9788577807383 â€” Ano: 2011) da Editora Bookman. </p>', '80d172edd16c3e72166685de9b3f3a4a671aea4c-his.jpg', ''),
(42, 'Raphael Rezende de Oliveira', 'Lenovo lanÃ§a notebooks Thinkpad com processadores Sandy Bridge', '', 'Segunda-Feira, 07 de Maio de 2012, Ã s 17:09', '<p>A leva de lanÃ§amentos de computadores com os novos processadores Intel Ivy Bridge continua. A Lenovo anunciou oficialmente a chegada dos novos modelos <b>ThinkPad E430</b> e <b>E530</b>. Apesar de serem ultrabooks, os aparelhos sÃ£o tÃ£o espessos quanto os notebooks convencionais.</p>\r\n\r\n<p>Os lanÃ§amentos mantÃªm as caracterÃ­sticas estruturais presentes na famÃ­lia ThinkPad, como o trackpad emborrachado e botÃµes do touchpad na parte superior. O design conta com uma aparÃªncia sÃ³bria e profissional e Ã© ideal atividades educacionais e ambientes domÃ©sticos.</p>\r\n\r\n<p>Em comum, os novos portÃ¡teis da Lenovo possuem um processador Intel Core i3-2350M de 2.3 GHz, 4 GB de RAM e 320 GB de armazenamento em HD. Os modelos se diferem no tamanho da tela. Enquanto o E430 tem 14 polegadas, o E530 possui 15 polegadas. A resoluÃ§Ã£o do display Ã© a mesma (1.366 x 768 pÃ­xels) e o sistema operacional Ã© o Windows 7.</p>\r\n\r\n<p>O preÃ§o inicial dos dois modelos Ã© de US$ 549 (cerca de R$ 1.054) e o usuÃ¡rio poderÃ¡ escolher entre diversas configuraÃ§Ãµes disponÃ­veis. AtÃ© o momento, nÃ£o hÃ¡ previsÃ£o de lanÃ§amento dos modelos E430 e E530 no Brasil.</p>\r\n\r\nThinkPad E430 e E530 (Foto: DivulgaÃ§Ã£o):', '4ae575e9088e94a7bcbc6fd80db92f7cec415369.jpg', ''),
(43, 'Raphael Rezende de Oliveira', 'SanDisk traz para o Brasil o mini pen drive Cruzer Fit', 'Teste', 'Segunda-Feira, 07 de Maio de 2012, Ã s 17:17', '<p>O mini pen drive Cruzer Fit, da SanDisk, estÃ¡ chegando ao Brasil. O dispositivo de apenas 5,1 milÃ­metros, alÃ©m de guardar arquivos, expandirÃ¡ a capacidade de armazenamento de tablets e notebooks ultrafinos.</p>\r\n\r\n<p><b>O Cruzer Fit Ã© uma Ã³tima opÃ§Ã£o para os usuÃ¡rios que tÃªm preocupaÃ§Ãµes com a seguranÃ§a. O pen drive vem com proteÃ§Ã£o por senha graÃ§as ao software SanDisk Secure Access, alÃ©m de 2 GB de backup online (opcional).</b></p>\r\n\r\n<p>O produto tem cinco anos de garantia e estÃ¡ disponÃ­vel em trÃªs versÃµes: com 4 GB, 8 GB e 16 GB de espaÃ§o. Os preÃ§os sugeridos para o Cruzer Fit sÃ£o de R$ 24,90 (4GB) e R$ 39,90 (8GB).</p>\r\n\r\nSanDisk estÃ¡ chegando ao Brasil (Foto: DivulgaÃ§Ã£o)', 'f1a260709c1fdf9ad57216380a0ff590b55b2980.jpg', ''),
(44, 'Raphael Rezende de Oliveira', 'Que 4K, que nada! Direto do JapÃ£o, um protÃ³tipo de plasma 8K', 'Teste', 'Segunda-Feira, 07 de Maio de 2012, Ã s 17:32', '<p>Parece mesmo que o reinado da resoluÃ§Ã£o <b>Full-HD (1920x1080p)</b> estÃ¡ com os dias contados. Grande atraÃ§Ã£o nas feiras internacionais (ainda quase sempre sob a forma de protÃ³tipo), as telas 4K (com definiÃ§Ã£o 4x maior, ou seja, 4096Ã—2160 pixels) devem invadir o mercado internacional em breve, provocando uma nova revoluÃ§Ã£o na qualidade da imagem.</p>\r\n\r\n<p>Mas quem estÃ¡ falando de 4K? A Panasonic, em parceria com a rede de TV NHK, apresentou no JapÃ£o um protÃ³tipo de tela de plasma 2D com 145 polegadas e resoluÃ§Ã£o 8K, 16x mais detalhada que a Full-HD. Para quem gosta de nÃºmeros, sÃ£o quase 34 milhÃµes de pixels, 7.860 horizontais por 4.320 verticais.</p>\r\n\r\n<p>Para deixar as cenas mais reais, o display utiliza uma nova tecnologia de estabilizaÃ§Ã£o da imagem, baseada no escaneamento simultÃ¢neo dos pixels na direÃ§Ã£o vertical. Com isso, diz o informativo Akihabara News, foi possÃ­vel vencer as cintilaÃ§Ãµes em um painel com cerca de 4.000 linhas de varredura. Claro que o foco do produto nÃ£o Ã© o mercado domÃ©stico, mas sim as aplicaÃ§Ãµes comerciais. Quer conhecer melhor a novidade? Ã‰ sÃ³ clicar no vÃ­deo acima!</p>', 'a3832e3c7a8c19f48fa640f6252aecd38e00108a.jpg', 'uoEj_wIkJzU'),
(46, 'Iuri Cavalcante', 'Como utilizar programas do Windows no Ubuntu', 'Linux', 'TerÃ§a-Feira, 08 de Maio de 2012, Ã s 15:54', '<p>Como Ã© sabido, o Ubuntu Ã© uma distribuiÃ§Ã£o GNU/Linux completamente independente do Windows, o que significa que os programas do Windows sÃ£o totalmente incompatÃ­veis com o Ubuntu.</p>\r\n\r\n<p>No entanto atravÃ©s de um aplicativo chamado Wine, Ã© possÃ­vel fazer com que programas nativos do Windows possam ser executados no Ubuntu, com uma performance bastante atrativa uma vez que os programas nÃ£o estÃ£o a ser emulados mas a serem executados atravÃ©s de cÃ³digo equivalente ao do Windows.</p>\r\n\r\n<p>Neste artigo apresentaremos o Wine (sigla recursiva de Wine Is Not an Emulator, que significa â€œWine nÃ£o Ã© um emuladorâ€), um programa que disponibiliza bibliotecas de ficheiros equivalentes Ã s do Windows, permitindo assim que os programas nativos do Windows sejam executados no Ubuntu.</p>\r\n\r\n<p><b>InstalaÃ§Ã£o</b></p>\r\n\r\n<p>Apesar de uma versÃ£o do Wine se encontrar nos repositÃ³rios do Ubuntu, ela nÃ£o Ã© recente e poderÃ¡ nÃ£o suportar alguns programas do Windows (principalmente os mais atuais). Com a adiÃ§Ã£o de um repositÃ³rio o Wine que instalarÃ¡ no seu computador serÃ¡ a versÃ£o mais atual disponÃ­vel.</p>\r\n\r\n<p>Inicie o Terminal do Ubuntu (atravÃ©s do Dash) e insira o seguinte comando:\r\nsudo add-apt-repository ppa:ubuntu-wine/ppa -y && sudo apt-get update</p>\r\n\r\n<p>Assim que a atualizaÃ§Ã£o de repositÃ³rios por parte do Ubuntu terminar poderÃ¡ iniciar a instalaÃ§Ã£o com o seguinte comando:</p>\r\n<p><b>sudo apt-get install wine</b></p>\r\n\r\n \r\n<p><b>Ubuntu 64bits (para casos excecionais)</b></p>\r\n\r\n<p>Caso nÃ£o consiga executar o Wine, verifique se a versÃ£o do seu Ubuntu Ã© a de 64 bits atravÃ©s do procedimento explicado neste artigo. Se assim se verificar execute o seguinte comando e o Wine ficarÃ¡ a funcionar.\r\nsudo apt-get install ia32-libs libc6-i386</p>\r\n\r\n \r\n<p><b>ConfiguraÃ§Ãµes</b></p>\r\n<p><b>Pasta C:</b></p>\r\n\r\n<p>Se a qualquer altura da instalaÃ§Ã£o de um programa no Wine necessitar de alterar alguns dos seus ficheiros encontrarÃ¡ a pasta relativa ao â€œDisco C:â€ virtual na seguinte diretoria:</p>\r\n\r\n   <p><b> ~/.wine/drive_c</b></p>\r\n\r\n<p>O sÃ­mbolo â€˜~â€™ indica Pasta Pessoal, ou seja, a pasta â€œ.wineâ€ encontra-se escondida na sua Pasta Pessoal, podendo ser necessÃ¡rio utilizar a combinaÃ§Ã£o de teclas â€œCtrl+Hâ€ para a visualizar.</p>\r\n\r\n \r\n<p><b>WineCfg</b></p>\r\n\r\n<p>Ã‰ recomendÃ¡vel utilizar a ferramenta de auto-deteÃ§Ã£o de drives do WineCfg, desta forma o Wine encontrarÃ¡ e ficarÃ¡ capaz de ler ficheiros a partir da drive de CDâ€™s/DVDâ€™s e tambÃ©m ficheiros do tipo ISO, caso os tenha montados.</p>\r\n\r\n<p>EncontrarÃ¡ este programa atravÃ©s do Unity/Gnome-Shell com o nome â€œwinecfgâ€ ou â€œConfiguraÃ§Ã£o Wineâ€œ. Nele clique em â€œUnidadesâ€ seguido de â€œAutodetectarâ€œ.</p>\r\n\r\n<p><b>Instalar Programas</b></p>\r\n\r\n<p>A instalaÃ§Ã£o de programas Ã© bastante simples, transfira o ficheiro de instalaÃ§Ã£o do programa que deseja, como exemplo utilizaremos o programa de distribuiÃ§Ã£o de jogos Steam, e faÃ§a duplo clique nele. Depois apenas terÃ¡ de seguir as instruÃ§Ãµes de instalaÃ§Ã£o do Steam.</p>\r\n\r\n<p><b>Desinstalar Programas</b></p>\r\n\r\n<p>O Wine providÃªncia uma excelente aplicaÃ§Ã£o que permite desinstalar programas sem qualquer dificuldade. Esta aplicaÃ§Ã£o chama-se â€œDesinstalar AplicaÃ§Ãµes do Wineâ€ e pode ser acedida atravÃ©s do Unity ou do Gnome-Shell. </p>', 'd997d7736071ba8eea8b4f2ed34fcbcaf2696c34.jpg', 'c9Aeq6TZwXQ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicidade`
--

CREATE TABLE IF NOT EXISTS `publicidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anunciante` varchar(45) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `imagem` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `publicidade`
--

INSERT INTO `publicidade` (`id`, `anunciante`, `link`, `imagem`) VALUES
(18, 'De Bem com o Planeta', 'http://debemcomoplaneta.com.br', '55c1f4feb2172328ee247ec28380d56b108d9e6f.jpg'),
(19, 'Firefox', 'http://www.baixaki.com.br/download/mozilla-firefox.htm', '83c48b7d07d7a82be44a4e6a317e30dd51f818b5.jpg'),
(21, 'JustiÃ§a', 'http://fellowship.birn.eu.com/en/fellowship-programme/fellowship-programme-topic-2011-justice', '98b6721eba42892d7baf8d8004316e582b18e442.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
