<?php

class Post {
    private $id;
    private $autor;
    private $categoria;
    private $titulo;
    private $post;
    private $data;
    private $imagem;
    private $video;

    function incluir(){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
                
        $sql = "INSERT INTO";
        $sql .= " post (autor,titulo,categoria,data,post,imagem,video) ";
        $sql .= " VALUES ('$this->autor','$this->titulo','$this->categoria','$this->data','$this->post','$this->imagem','$this->video') ";
        
        $obj->set('sql', $sql);
        $obj->query();
        
        return TRUE;
    }
    
    function excluir(){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
        
        $sql = "DELETE FROM post ";
        $sql .= " WHERE id=$this->id ";
        
        $obj->set('sql', $sql);
        $obj->query();
    }
    
    function consultar($tipo){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
        
        $post = "";
        
        if($tipo == "ver"){
            $sql = "SELECT * FROM post ";
            $sql .= " ORDER  BY CONVERT( id, UNSIGNED )  DESC ";
        }       

        $obj->set('sql', $sql);
        $result = $obj->query();
        $i=0;
        while ($myrow = mysql_fetch_array($result)){
            $i++;
            $post[$i]['id'] = $myrow["id"];
            $post[$i]['autor'] = $myrow["autor"];
            $post[$i]['categoria'] = $myrow["categoria"];
            $post[$i]['titulo'] = $myrow["titulo"];
            $post[$i]['data'] = $myrow["data"];
            $post[$i]['post'] = $myrow["post"];
            $post[$i]['imagem'] = $myrow["imagem"];
            $post[$i]['video'] = $myrow["video"];
        }
        
        $obj->fechaconexao();
        
        return $post;
    }
    
    function consultarPost($inicio,$fim){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
        
        $post = "";
        
        $sql = "SELECT * FROM post ";
        $sql .= " ORDER  BY CONVERT( id, UNSIGNED )  DESC LIMIT $inicio,$fim ";
        
        $obj->set('sql', $sql);
        $result = $obj->query();
        $i=0;
        while ($myrow = mysql_fetch_array($result)){
            $i++;
            $post[$i]['id'] = $myrow["id"];
            $post[$i]['autor'] = $myrow["autor"];
            $post[$i]['categoria'] = $myrow["categoria"];
            $post[$i]['titulo'] = $myrow["titulo"];
            $post[$i]['data'] = $myrow["data"];
            $post[$i]['post'] = $myrow["post"];
            $post[$i]['imagem'] = $myrow["imagem"];
            $post[$i]['video'] = $myrow["video"];
        }
        
        $obj->fechaconexao();
        
        return $post;
    }

    function set($prop, $value){
        $this->$prop = $value;
    }
    
    function get($prop){
        return $this->$prop;
    }
}

?>
