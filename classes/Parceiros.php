<?php

class Parceiros {
    private $id;
    private $email;
    private $nome;
    private $link;
    private $responsavel;
    private $tipo;

    function incluir(){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
                
        $sql = "INSERT INTO";
        $sql .= " parceiros (nome,responsavel,email,link,tipo) ";
        $sql .= " VALUES ('$this->nome','$this->responsavel','$this->email','$this->link','$this->tipo') ";
        
        $obj->set('sql', $sql);
        $obj->query();
        
        return TRUE;
    }
    
    function alterar(){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
               
        $sql = "UPDATE parceiros ";
        $sql .= " SET nome='$this->nome', responsavel='$this->responsavel', email='$this->email', link='$this->link', tipo='$this->tipo' ";
        $sql .= " WHERE id=$this->id";
                       
        $obj->set('sql', $sql);
        $obj->query();
        
        return TRUE;
    }
    
    function excluir(){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
        
        $sql = "DELETE FROM parceiros ";
        $sql .= " WHERE id=$this->id ";
        
        $obj->set('sql', $sql);
        $obj->query();
        
        return true;
    }
    
    function consultar(){
        $obj = new Conexao();
        $obj->getConexao();
        $obj->selecionarDB();
        
        $parc = "";
        
        $sql = "SELECT * FROM parceiros ";

        $obj->set('sql', $sql);
        $result = $obj->query();
        $i=0;
        while ($myrow = mysql_fetch_array($result)){
            $i++;
            $parc[$i]['id'] = $myrow["id"];
            $parc[$i]['nome'] = $myrow["nome"];
            $parc[$i]['responsavel'] = $myrow["responsavel"];
            $parc[$i]['email'] = $myrow["email"];
            $parc[$i]['link'] = $myrow["link"];
            $parc[$i]['tipo'] = $myrow["tipo"];
        }
        
        $obj->fechaconexao();
        
        return $parc;
    }
    
    function set($prop, $value){
        $this->$prop = $value;
    }
    
    function get($prop){
        return $this->$prop;
    }
}

?>
