<?php

class funcoes {
    
    function uploads($destino, $nome, $largura, $pasta, $tipo){
        $ext = strtolower(end(explode('.',$nome)));
        
        if($ext == 'jpg'){
		$img = imagecreatefromjpeg($destino);
	}elseif($ext =='gif'){
		$img = imagecreatefromgif($destino);
	}else{
		$img = imagecreatefrompng($destino);
	}
        
        
        $x = imagesx($img);
        $y = imagesy($img);
        $altura = ($largura * $y) / $x;
        
        $novaImagem = imagecreatetruecolor($largura, $altura);
        imagecopyresampled($novaImagem, $img, 0, 0, 0, 0, $largura, $altura, $x, $y);
        imagejpeg($novaImagem, "$pasta/$nome");
        
        imagedestroy($img);
        imagedestroy($novaImagem);
    }


    function contar($id_c){
        $coment = new Comentarios();
        $a=0;
        if($coment->consultar() != null){
            foreach ($coment->consultar() as $valor){
                if($id_c==$valor['id_post']){
                    $a++;
                }    
            }
    }
    return $a;
    }
    function upload($tmp, $name, $nome, $larguraP, $pasta){
	
	$ext = strtolower(end(explode('.',$name)));
	
	if($ext == 'jpg'){
		$img = imagecreatefromjpeg($tmp);
	}elseif($ext =='gif'){
		$img = imagecreatefromgif($tmp);
	}else{
		$img = imagecreatefrompng($tmp);
	}
	
	$x = imagesx($img);
	$y = imagesy($img);

	$largura = ($x>$larguraP) ? $larguraP : $x;
	$altura = ($largura*$y) / $x;
	
	if($altura>$larguraP){
		$altura = $larguraP;
		$largura = ($altura*$x) / $y;
	}
	
	$nova = imagecreatetruecolor($largura, $altura);
	imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura, $altura, $x, $y);
	imagejpeg($nova, "$pasta/$nome");
        imagedestroy($img);
	imagedestroy($nova);
	
	return $nome;
    }
    
    function data(){
        date_default_timezone_set('America/Sao_Paulo');
        
        $dia_semana = date("D");
        $dia = date("d");
        $mes = date("m");
        $ano = date("Y");
        $hora = date("H:i");
        
        switch ($dia_semana){
            case 'Mon':
                $dia_semana = "Segunda-Feira";
                break;
            case 'Tue':
                $dia_semana = "Terça-Feira";
                break;
            case 'Wed':
                $dia_semana = "Quarta-Feira";
                break;
            case 'Thu':
                $dia_semana = "Quinta-Feira";
                break;
            case 'Fri':
                $dia_semana = "Sexta-Feira";
                break;
            case 'Sat':
                $dia_semana = "Sabado";
                break;
            case 'Sun':
                $dia_semana = "Domingo";
                break;

        }
        
        switch ($mes){
            case '1':
                $mes="Janeiro";
                break;
            case '2':
                $mes="Fevereiro";
                break;
            case '3':
                $mes="Março";
                break;
            case '4':
                $mes="Abril";
                break;
            case '5':
                $mes="Maio";
                break;
            case '6':
                $mes="Junho";
                break;
            case '7':
                $mes="Julho";
                break;
            case '8':
                $mes="Agosto";
                break;
            case '9':
                $mes="Setembro";
                break;
            case '10':
                $mes="Outubro";
                break;
            case '11':
                $mes="Novembro";
                break;
            case '12':
                $mes="Dezembro";
                break;
        }
            
        $dataEhora = $dia_semana.", ".$dia." de ".$mes." de ".$ano.", às ".$hora;
        
        return $dataEhora;
    }
}

?>
