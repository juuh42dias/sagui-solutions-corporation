<?php
ini_set("display_errors", 0);

include_once '../classes/Conexao.php';
include_once '../classes/Publicidade.php';
include_once '../classes/funcoes.php';

$erro = "";

$id="";
$anunciante="";
$link="";
$imagem="";

if(!empty ($_POST)){
    $objeto = new Publicidade();
    $funcao = new funcoes();
    
    extract($_POST);
    if($txtValor == "gravar" ){
        
    if($_FILES['imagem']!=""){
        $pasta = '../imagens/publicidade';
        $permite = array('image/jpeg','image/pjpeg','image/jpg','image/gif','image/png');
        $imagem = $_FILES['imagem'];
        $destino = $imagem['tmp_name'];
        $nome = $imagem['name'];
        $tipo = $imagem['type'];
        
        if(in_array($tipo, $permite)){
            $funcao->uploads($destino, $nome, 620, $pasta, $tipo);
            $objeto->set('imagem', $nome);
        }else{
            echo "Imagem inválida";
        }
//        $imagem = $_FILES['imagem'];
//        $nome = sha1($imagem['name']).'.jpg';
//        $ext = array('image/jpeg','image/pjpeg','image/jpg','image/gif','image/png');
//            if(in_array($imagem['type'], $ext)){
//                $funcao->upload($imagem['tmp_name'],$imagem['name'],$nome,300,'../imagens/publicidade/');
//                $objeto->set('imagem', $nome);
//            }
        }
        $objeto->set('id', $id);
        $objeto->set('anunciante', $anunciante);
        $objeto->set('link', $link);
        
        if($txtValor =="gravar"){
            if($objeto->incluir()==true){
                $id="";
                $anunciante="";
                $link="";
                $imagem="";
            }  
        }
    }elseif ($txtValor == "excluir"){
         $objeto->set('id', $id);
         if($imagemF!=""){
            unlink("../imagens/publicidade/".$imagemF);
        }
        if($objeto->excluir()==true){
            $id="";
            $anunciante="";
            $link="";
            $imagem="";
            $imagemF="";
        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript" >
            function excluir(cod, imagem){
                if(confirm("Deseja mesmo deletar este parceiro?")){
                    document.frmCad.id.value = cod;
                    document.frmCad.imagemF.value = imagem;
                    document.frmCad.txtValor.value = "excluir";
                    document.frmCad.submit();
                }
            }
            
        </script>
    </head>
    <body>
        <form name="frmCad" method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="txtValor" value="gravar" />
            <input type="hidden" name="imagemF"  />
            <table>
                <tr><td colspan="2"><h1>Anunciantes</h1></td></tr>
                <tr><td><h3>Id*:</h3></td><td><input type="text" style="width: 35px;" name="id" readonly="true" value="<?php echo $id;?>" /></td></tr>
                <tr><td><h3>Anunciante:</h3></td><td><input type="text" name="anunciante" value="<?php echo $anunciante;?>"/></td></tr>
                <tr><td><h3>Link:</h3></td><td><input type="text" name="link" value="http://<?php echo $link;?>"/></td></tr>
                <tr><td><h3>Imagem:</h3></td><td><input type="file" name="imagem" value="<?php echo $imagem;?>"/></td></tr>
                <tr><td colspan="2" align="right"><input type="submit" value="Incluir"/> <input type="reset" value="Limpar"/> </td></tr>
            </table>
            <br/>
            <table  name="mostrar">
                <tr name="linha"><td ><h3>#</h3></td><td><h3>Anunciante</h3></td><td><h3>Link</h3></td><td><h3>Imagem</h3></td><td><h3>Excluir</h3></td></tr>
                <?php
                $count = 0;
                $publicidade = new Publicidade();
                if($publicidade->consultar() != null){
                    foreach ($publicidade->consultar() as $valor){
                        echo "<tr>";
                        echo "<td>". $count += 1 ."</td>";
                        echo "<td  class='coluna'>". $valor['anunciante']."</td>";
                        echo "<td>". $valor['link']."</td>";
                        echo "<td  class='coluna'>". $valor['imagem']."</td>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].",\"".$valor['imagem']."\")' /></td>";
                        echo "</tr>";
                    }
                }
                ?>
            </table>
        </form>
    </body>
</html>

