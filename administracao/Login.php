<?php
ini_set("display_errors", 1);

include_once '../classes/Conexao.php';
include_once '../classes/Administrador.php';

$erro = "";
setcookie("id", "");
//verificar se foi dado post na pagina
if(!empty ($_POST)){
    $objeto = new Administrador();
    
    $objeto->set('email', $_POST['usuario']);
    $objeto->set('senha', $_POST['senha']);
    
    if($objeto->verificarUsuario()){
        session_start();
        
        $_SESSION['usuario'] = $objeto;
        
        setcookie("id", $_SESSION['usuario']->get('id') , time()+6000);
        //Redireciona para outra pagina
        header("Location: telaAdmin.php");
    }else{
        //emite uma mensagem
        $erro = "Desculpe, nenhum registro encontrado!";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RNC 2</title>
        <link href="../estilos/cssRNC.css" type="text/css" rel="stylesheet"/>
    </head>
    <body>
        <div id="topo">
            <div class="topo">
                <img src="../imagens/Logo.png"/><img src="../imagens/msg.png" style="margin-top: 6px;"/>
            </div>
        </div>
        <div id="contentBotoes">
            <div class="botoes">
                <table style="width: 100%;">
                    <tr>
                        <td><h3><a href="../index.php">Blog</a> | <a href="#">Sobre o blog</a> | <a href="../EnviaPost.php?ok">Envie um post</a> | <a href="../contato.php?ok">Contato</a></h3></td>
                        <td align="right">
                            <div class="busca>">
                     <form method="GET" action="">
                        <p><input type="text" name="busca"/> <input type="submit" value="Pesquisar" /></p>
                    </form>
                </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="content">
            
        <form name="login" method="post" enctype="multipart/form-data" action="">
            <div id="login">
                <table border="0">
                    <tr><td><h1>Área Restrita</h1></td></tr>
                    <tr><td><p class="erro"><b><?php echo $erro;?></b></p></td></tr>
                    <tr><td><h2>Usuário:</h2></td></tr>
                    <tr><td><input id="t" type="text" name="usuario" /></td></tr>
                    <tr><td><h2>Senha:</h2></td></tr>
                    <tr><td><input id="t" type="password" name="senha" /></td></tr>
                    <tr><td><br/></td></tr>
                    <tr><td><p align="right"><input type="submit" name="logar" value="Entrar"/> <input type="reset" name="logar" value="Limpar"/></p></td></tr>
                </table>
            </div><!--login-->
        </form> 
                
</div><!--content-->

      <div id="footer">
            <div class="footer">
            <table border="0">
             <tr><td><div class="footerFundoimg">
                         <img src="../imagens/LogoPe.png"/>
                 </div></td>
                 <td valign="top">
                     <div class="footerFundo">
                     <a href="../index.php">Blog</a>
                     <a href="../EnviaPost.php?ok">Envie seu post</a>
                     <a href="../contato.php?ok">Contato</a>
                     <a href="#">Sobre o blog</a>
                     <a href="#">Anuncie</a>
                     <a href="../administracao/Login.php">Administração</a>
                     <hr/>
                     <p>RedNerd Crew - <?php echo date('Y');?> - Alguns direitos reservados. O conteúdo deste blog é autoria de Osvaldo Carlos e de seus colaboradores. Nenhum conteúdo deste site pode ser copiado e/ou reproduzido em outro site sem autorização do autor!</p>
                     <hr/>
                     <p>Powered by: Raphael R. Oliveira - Made in Brazil - Goiás</p>
                     </div>
                 </td></tr>
         </table>
            </div>
        </div>
    </body>
</html>