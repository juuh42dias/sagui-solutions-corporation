<?php
ini_set("display_errors", 0);

include_once '../classes/Conexao.php';
include_once '../classes/Post.php';
include_once '../classes/funcoes.php';
include_once '../classes/Categorias.php';

if(!empty ($_POST)){
    $objeto = new Post();
    $funcao = new funcoes();
    
    if($_FILES['imagem']!=""){
        
        $pasta = '../imagens/uploads';
        $permite = array('image/jpeg','image/pjpeg','image/jpg','image/gif','image/png');
        $imagem = $_FILES['imagem'];
        $destino = $imagem['tmp_name'];
        $nome = $imagem['name'];
        $tipo = $imagem['type'];
        
        if(in_array($tipo, $permite)){
            $funcao->uploads($destino, $nome, 620, $pasta, $tipo);
            $objeto->set('imagem', $nome);
        }else{
            echo "Imagem inválida";
        }
        
//    $imagem = $_FILES['imagem'];
//    $nome = $imagem['name'];
//    $ext = array('image/jpeg','image/pjpeg','image/jpg','image/gif','image/png');
//        if(in_array($imagem['type'], $ext)){
//            $funcao->upload($imagem['tmp_name'],$imagem['name'],$nome,700,'../imagens/uploads');
//            $objeto->set('imagem', $nome);
//        }
        
    }
    
    $objeto->set('autor', $_POST['autor']);
    $objeto->set('categoria', $_POST['categoria']);
    $objeto->set('titulo', $_POST['titulo']);
    $objeto->set('post', $_POST['post']);
    $objeto->set('data', $funcao->data());
    $objeto->set('video', $_POST['video']);
    
    if($objeto->incluir()==true){
        
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
    </head>
    <body>
        <form method="POST" action="" enctype="multipart/form-data">
            <table>
                <tr><td colspan="2"><h1>Postagem</h1></td></tr>
                <tr><td><h3>Autor:</h3></td><td><input type="text" name="autor"/></td></tr>
                <tr><td><h3>Categoria:</h3></td><td>
                        <select name="categoria" style="width: 300px; height: 30px; font-size: 19px; border-color: black;">
                            <option selected="select"></option>
                            <?php
                            $categoria = new Categorias();
                            if($categoria->consultar() != null){
                                foreach ($categoria->consultar() as $valor){
                                    echo "<option>". $valor['nome']."</option>";
                                }
                            }
                            ?></option>
                        </select>
                        
                    </td></tr>
                <tr><td><h3>Titulo do Post:</h3></td><td><input type="text" name="titulo"/></td></tr>
                <tr><td><h3>Imagem:</h3></td><td><input type="file" name="imagem" style="width: 300px; height: 30px; font-size: 19px; border-color: black;"/></td></tr>
                <tr><td><h3>Código do Video:</h3></td><td><input type="text" name="video" /></td></tr>
                <tr><td><h3 align="center">Texto:</h3></td><td><textarea name="post"></textarea></td></tr>
                <tr><td colspan="2" align="right"><input type="submit" value="Postar"/> <input type="reset" value="Limpar"/> </td></tr>
            </table>
        </form>
    </body>
</html>
