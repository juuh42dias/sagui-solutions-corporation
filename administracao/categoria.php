<?php
ini_set("display_errors", 1);

include_once '../classes/Conexao.php';
include_once '../classes/Categorias.php';

$erro = "";
$id="";
$nome="";

if(!empty ($_POST)){
    $objeto = new Categorias();
    
    extract($_POST);
    if($txtValor == "gravar" || $txtValor == "alterar"){
        
        $objeto->set('id', $id);
        $objeto->set('nome', $nome);
        
        if($txtValor =="gravar"){
            if($objeto->incluir()==true){
                $id="";
                $nome="";
            }  
        }elseif ($txtValor == "alterar") {
            if($objeto->alterar()==true){
                $id="";
                $nome="";
            }
        }
    }
    if ($txtValor == "excluir"){
         $objeto->set('id', $id);
        if($objeto->excluir()==true){
            $id="";
            $nome="";
        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript" >
            function excluir(cod){
                if(confirm("Deseja mesmo deletar esta categoria?")){
                    document.frmCad.id.value = cod;
                    document.frmCad.txtValor.value = "excluir";
                    document.frmCad.submit();
                }
            }
            function alterar(id, nome){
                document.frmCad.id.value = id;
                document.frmCad.nome.value = nome;
                document.frmCad.txtValor.value = "alterar";
            }
            
        </script>
    </head>
    <body>
        <form name="frmCad" action="#" method="POST">
            <input type="hidden" name="txtValor" value="gravar" />
            <table>
                <tr><td colspan="2"><h1>Categorias</h1></td></tr>
                <tr><td><h3>Id*:</h3></td><td><input type="text" style="width: 35px;" name="id" readonly="true" value="<?php echo $id;?>" /></td></tr>
                <tr><td><h3>Nome:</h3></td><td><input type="text" name="nome" value="<?php echo $nome;?>"/></td></tr>
                <tr><td colspan="2" align="right"><input type="submit" value="Incluir"/> <input type="reset" value="Limpar"/> </td></tr>
            </table>
            <br/>
            <table  name="mostrar">
                <tr name="linha"><td ><h3>#</h3></td><td><h3>Nome</h3></td><td><h3>Editar</h3></td><td><h3>Excluir</h3></td></tr>
                <?php
                $count = 0;
                $categoria = new Categorias();
                if($categoria->consultar() != null){
                    foreach ($categoria->consultar() as $valor){
                        echo "<tr>";
                        echo "<td>". $count += 1 ."</td>";
                        echo "<td  class='coluna'>". $valor['nome']."</td>";
                        echo "<td><input type='button' value='Alterar' onclick='alterar(".$valor['id'].",\"".$valor['nome']."\")' /></td>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].")' /></td>";
                        echo "</tr>";
                    }
                }
                ?>
            </table>
        </form>
    </body>
</html>
