<?php
ini_set("display_errors", 1);

include_once '../classes/Conexao.php';
include_once '../classes/Newsletter.php';

$erro = "";
if(!empty ($_POST)){
    $objeto = new Newsletter();
    
    $objeto->set('id', $_POST['id']);
    
    if($objeto->excluir()==true){
        
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript">
            function excluir(id){
                if(confirm("Deseja mesmo deletar este post?")){
                    document.frmEx.id.value = id;
                    document.frmEx.submit();
                }
                
            }
        </script>
    </head>
    <body>
        <form method="POST" action="" name="frmEx">
            <input type="hidden" name="id" />
            <table  name="mostrar">
                <tr><td colspan="4"><h1>Úsuarios na newsletter</h1></td></tr>
                <tr  name="linha"><td><h3>#</h3></td><td><h3>E-mail</h3></td><td><h3>Excluir</h3></td></tr>
                <?php
                $count = 0;
                $news = new Newsletter();
                if($news->consultar() != null){
                    foreach ($news->consultar() as $valor){
                        echo "<tr>";
                        echo "<td>". $count += 1 ."</td>";
                        echo "<td  class='coluna'>".$valor['email']."</h1>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].")' /></td>";
                        echo "</tr>";
                    }
                }
            ?>
            </table>
        </form>
    </body>
</html>
