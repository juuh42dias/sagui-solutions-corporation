<?php
ini_set("display_errors", 0);

include_once '../classes/Conexao.php';
include_once '../classes/EnviarPost.php';

if(!empty ($_POST)){
    $objeto = new EnviarPost();
    $objeto->set('id', $_POST['id']);
    if($_POST['imagem']!=""){
        unlink("../imagens/uploads/".$_POST['imagem']);
    }
    
    $objeto->excluir();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript">
            function excluir(id,imagem){
                if(confirm("Deseja mesmo deletar este post?")){
                    document.frmEx.id.value = id;
                    document.frmEx.imagem.value = imagem;
                    document.frmEx.submit();
                }
                
            }
        </script>
    </head>
    <body>
        <form method="POST" action="" name="frmEx">
            <input type="hidden" name="id" />
            <input type="hidden" name="imagem" />
            <table  name="mostrar">
                <tr><td colspan="6"><h1>Posts Enviados</h1></td></tr>
                <tr  name="linha"><td><b>Titulo</b></td><td><b>Autor</b></td><td><b>Data</b></td><td><b>Categoria</b></td><td><b>Excluir</b></td><td><b>Ver</b></td></tr>
                <?php
                $post = new EnviarPost();
                if($post->consultar() != null){
                    foreach ($post->consultar() as $valor){
                        echo "<tr>";
//                        echo "<td><img src='../imagens/".$valor['imagem']."' /></td>";
                        echo "<td class='coluna'>".$valor['titulo']."</td>";
                        echo "<td>".$valor['nome']."</td>";
                        echo "<td class='coluna'>".$valor['data']."</td>";
                        echo "<td>".$valor['categoria']."</td>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].",\"".$valor['imagem']."\")' /></td>";
                        echo "<td><input type='button' value='Ver' /></td>";
                        echo "</tr>";
                    }
                }
            ?>
            </table>
        </form>
    </body>
</html>

