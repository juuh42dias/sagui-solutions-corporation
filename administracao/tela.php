<?php
ini_set("display_errors", 1);
include_once ('../classes/Administrador.php');
session_start();
$usuario = $_SESSION["usuario"];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
    </head>
    <body>
        <table border="1" name="mostrar">
            <tr><td colspan="2"><h1>Bem Vindo</h1></td></tr>
            <tr><td><h3>Nome:</h3></td><td><?php echo $usuario->get('nome');?></td></tr>
            <tr><td><h3>Email:</h3></td><td><?php echo $usuario->get('email');?></td></tr>
            <tr><td><h3>Obs:</h3></td><td><?php echo $usuario->get('obs');?></td></tr>
        </table>
    </body>
</html>
