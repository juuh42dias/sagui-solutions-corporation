<?php
ini_set("display_errors", 0);

include_once '../classes/Conexao.php';
include_once '../classes/Post.php';


if(!empty ($_POST)){
    $objeto = new Post();
    $objeto->set('id', $_POST['id']);
    if($_POST['imagem']!=""){
//        delete();
        unlink("../imagens/uploads/".$_POST['imagem']);
    }
    
    if($objeto->excluir()==true){
        
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript">
            function excluir(id,imagem){
                if(confirm("Deseja mesmo deletar este post?")){
                    document.frmEx.id.value = id;
                    document.frmEx.imagem.value = imagem;
                    document.frmEx.submit();
                }
                
            }
        </script>
    </head>
    <body>
        <form method="POST" action="" name="frmEx">
            <input type="hidden" name="id" />
            <input type="hidden" name="imagem" />
            <table  name="mostrar">
                <tr><td colspan="6"><h1>Posts</h1></td></tr>
                <tr  name="linha"><td><b> # </b></td><td><b>Titulo</b></td><td><b>Autor</b></td><td><b>Data</b></td><td><b>Categoria</b></td><td><b>Excluir</b></td></tr>
                <?php
                $count=1;
                $post = new Post();
                if($post->consultar("ver") != null){
                    foreach ($post->consultar("ver") as $valor){
                        echo "<tr>";
                        echo "<td class='coluna'> ".$count++."</td>";
                        echo "<td>".$valor['titulo']."</h1>";
                        echo "<td class='coluna'>".$valor['autor']."</h2>";
                        echo "<td>".$valor['data']."</h2>";
                        echo "<td class='coluna'>".$valor['categoria']."</h2>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].",\"".$valor['imagem']."\")' /></td>";
                        echo "</tr>";
                    }
                }
            ?>
            </table>
        </form>
    </body>
</html>
