<?php
ini_set("display_errors", 1);

include_once '../classes/Conexao.php';
include_once '../classes/Parceiros.php';

$erro = "";
$id="";
$nome="";
$responsavel="";
$email="";
$link="";
$tipo="";

if(!empty ($_POST)){
    $objeto = new Parceiros();
    
    extract($_POST);
    if($txtValor == "gravar" || $txtValor == "alterar"){
        
        $objeto->set('id', $id);
        $objeto->set('nome', $nome);
        $objeto->set('responsavel', $responsavel);
        $objeto->set('email', $email);
        $objeto->set('link', $link);
        $objeto->set('tipo', $tipo);
        
        if($txtValor =="gravar"){
            if($objeto->incluir()==true){
                $id="";
                $nome="";
                $responsavel="";
                $email="";
                $link="";
                $tipo="";
            }  
        }elseif ($txtValor == "alterar") {
            if($objeto->alterar()==true){
                $id="";
                $nome="";
                $responsavel="";
                $email="";
                $link="";
                $tipo="";
            }
        }
    }
    if ($txtValor == "excluir"){
         $objeto->set('id', $id);
        if($objeto->excluir()==true){
            $id="";
            $nome="";
            $responsavel="";
            $email="";
            $link="";
            $tipo="";
        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript" >
            function excluir(cod){
                if(confirm("Deseja mesmo deletar este parceiro?")){
                    document.frmCad.id.value = cod;
                    document.frmCad.txtValor.value = "excluir";
                    document.frmCad.submit();
                }
            }
            function alterar(id, nome, resp, email, link, tipo){
                document.frmCad.id.value = id;
                document.frmCad.nome.value = nome;
                document.frmCad.responsavel.value = resp;
                document.frmCad.email.value = email;
                document.frmCad.link.value = link;
                document.frmCad.tipo.value = tipo;
                document.frmCad.txtValor.value = "alterar";
            }
            
        </script>
    </head>
    <body>
        <form name="frmCad" method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="txtValor" value="gravar" />
            <table>
                <tr><td colspan="2"><h1>Sites ou Blogs Parceiros</h1></td></tr>
                
                <tr><td><h3>Id*:</h3></td><td><input type="text" style="width: 35px;" name="id" readonly="true" value="<?php echo $id;?>" /></td></tr>
                <tr><td><h3>Nome:</h3></td><td><input type="text" name="nome" value="<?php echo $nome;?>"/></td></tr>
                <tr><td><h3>Atuação:</h3></td><td><input type="text" name="tipo" value="<?php echo $tipo;?>"/></td></tr>
                <tr><td><h3>Responsável:</h3></td><td><input type="text" name="responsavel" value="<?php echo $responsavel;?>"/></td></tr>
                <tr><td><h3>E-mail:</h3></td><td><input type="text" name="email" value="<?php echo $email;?>"/></td></tr>
                <tr><td><h3>Link:</h3></td><td><input type="text" name="link" value="http://<?php echo $link;?>"/></td></tr>
                <tr><td colspan="2" align="right"><input type="submit" value="Incluir"/> <input type="reset" value="Limpar"/> </td></tr>
            </table>
            <br/>
            <table  name="mostrar">
                <tr name="linha"><td ><h3>#</h3></td><td><h3>Nome</h3></td><td><h3>Atuação</h3></td><td><h3>Responsável</h3></td><td><h3>E-mail</h3></td><td><h3>Link</h3></td><td><h3>Editar</h3></td><td><h3>Excluir</h3></td></tr>
                <?php
                $count = 0;
                $parc = new Parceiros();
                if($parc->consultar() != null){
                    foreach ($parc->consultar() as $valor){
                        echo "<tr>";
                        echo "<td>". $count += 1 ."</td>";
                        echo "<td  class='coluna'>". $valor['nome']."</td>";
                        echo "<td>". $valor['tipo']."</td>";
                        echo "<td  class='coluna'>". $valor['responsavel']."</td>";
                        echo "<td>". $valor['email']."</h1>";
                        echo "<td  class='coluna'>". $valor['link']."</td>";
                        echo "<td><input type='button' value='Alterar' onclick='alterar(".$valor['id'].",\"".$valor['nome']."\",\"".$valor['responsavel']."\",\"".$valor['email']."\",\"".$valor['link']."\",\"".$valor['tipo']."\")' /></td>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].")' /></td>";
                        echo "</tr>";
                    }
                }
                ?>
            </table>
        </form>
    </body>
</html>
