<?php
ini_set("display_errors", 1);

include_once '../classes/Conexao.php';
include_once '../classes/Administrador.php';

$erro = "";
$nome="";
$email="";
$senha="";
$conf_senha="";
$obs="";
$id="";
if(!empty ($_POST)){
    $admin = new Administrador();
    
    extract($_POST);
    
    
    
    if($txtValor == "gravar" || $txtValor == "alterar"){
        
    if($nome == ''){
        echo "<script type='text/javascript' >alert('Digite o nome!');</script>";
    }else if($email==''){
        echo "<script type='text/javascript' >alert('Digite o e-mail!');</script>";
    }elseif(!preg_match("/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/i",$email)){
        echo "<script type='text/javascript' >alert('Este e-mail é invalido!');</script>";
    }  elseif ($senha == '' or strlen($senha)<3) {
        echo "<script type='text/javascript' >alert('Digite a Senha Corretamente, com mais de três dígitos!!');</script>";
    }elseif($conf_senha != $senha){
        echo "<script type='text/javascript' >alert('A Confirmação da Senha está incorreta!');</script>";
    }else{
        $admin->set('id', $id);
        $admin->set('nome', $nome);
        $admin->set('email', $email);
        $admin->set('senha', $senha);
        $admin->set('obs', $obs);
        
        if($txtValor =="gravar"){
            if($admin->incluir()==true){
                $nome="";
                $email="";
                $senha="";
                $conf_senha="";
                $obs="";
                $id="";
                echo "<script type='text/javascript' >alert('Administrador cadastrado com sucesso!');</script>";
            }  
        }elseif ($txtValor == "alterar") {
            if($admin->alterar()==true){
                $nome="";
                $email="";
                $senha="";
                $conf_senha="";
                $obs="";
                $id="";
                echo "<script type='text/javascript' >alert('Administrador alterado com sucesso!');</script>";
            }
        }
         }
         
}
if ($txtValor == "excluir"){
             $admin->set('id', $id);
            if($admin->excluir()==true){
                $id="";
                echo "<script type='text/javascript' >alert('Administrador excluido com sucesso!');</script>";
            }
        }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" media="screen" href="../estilos/cssAdmin.css" type="text/css" />
        <script type="text/javascript" >
            function excluir(cod){
                if(confirm("Deseja mesmo deletar este administrador?")){
                    document.frmCad.id.value = cod;
                    document.frmCad.txtValor.value = "excluir";
                    document.frmCad.submit();
                }
            }
            function alterar(id, nome, email,obs){
                document.frmCad.id.value = id;
                document.frmCad.nome.value = nome;
                document.frmCad.email.value = email;
                document.frmCad.obs.value = obs;
                document.frmCad.txtValor.value = "alterar";
            }
            
        </script>
    </head>
    <body>
        <form name="frmCad" method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="txtValor" value="gravar" />
            <table>
                <tr><td colspan="2"><h1>Administradores</h1></td></tr>
                <tr><td><h3>Id*:</h3></td><td><input type="text" style="width: 35px;" name="id" readonly="true" value="<?php echo $id;?>" /></td></tr>
                <tr><td><h3>Nome:</h3></td><td><input type="text" name="nome" value="<?php echo $nome;?>"/></td></tr>
                <tr><td><h3>E-mail:</h3></td><td><input type="text" name="email" value="<?php echo $email;?>" /></td></tr>
                <tr><td><h3>Senha:</h3></td><td><input type="password" name="senha" /></td></tr>
                <tr><td><h3>Conf. Senha:</h3></td><td><input type="password" name="conf_senha" /></td></tr>
                <tr><td><h3>Observação:</h3></td><td><textarea name="obs"><?php echo $obs;?></textarea></td></tr>
                <tr><td colspan="2" align="center"><input type="submit" value="Cadastrar" /> <input type="reset" value="Limpar" /> </td></tr>
            </table>
            <br/>
            <table  name="mostrar">
                <tr name="linha"><td><h3>#</h3></td><td><h3>Nome</h3></td><td><h3>E-mail</h3></td><td><h3>Observação</h3></td><td><h3>Editar</h3></td><td><h3>Excluir</h3></td></tr>
                <?php
                $count = 0;
                $admin = new Administrador();
                if($admin->consultar() != null){
                    foreach ($admin->consultar() as $valor){
                        echo "<tr>";
                        echo "<td>". $count += 1 ."</td>";
                        echo "<td  class='coluna'>". $valor['nome']."</td>";
                        echo "<td>". $valor['email']."</h1>";
                        echo "<td  class='coluna'>". $valor['obs']."</td>";
                        echo "<td><input type='button' value='Alterar' onclick='alterar(".$valor['id'].",\"".$valor['nome']."\",\"".$valor['email']."\",\"".$valor['obs']."\")' /></td>";
                        echo "<td><input type='button' value='Excluir' onclick='excluir(".$valor['id'].")' /></td>";
                        echo "</tr>";
                    }
                }
                ?>
            </table>
            
        </form>
    </body>
</html>
