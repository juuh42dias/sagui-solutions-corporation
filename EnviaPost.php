<?php 
ini_set("display_errors", 1);

include_once 'includes/header.php';
include_once 'classes/EnviarPost.php';
include_once 'classes/funcoes.php';

$nome="";
$email="";
$catg="";
$titulo="";
$msg="";
$video="";
$texto="";
if(!empty ($_POST)&& $_POST['envio']=="enviodepost"){
    $objeto = new EnviarPost();
    $funcao = new funcoes();
    
      if($_FILES['imagem']!=""){
        
        $pasta = 'imagens/uploads';
        $permite = array('image/jpeg','image/pjpeg','image/jpg','image/gif','image/png');
        $imagem = $_FILES['imagem'];
        $destino = $imagem['tmp_name'];
        $nome = $imagem['name'];
        $tipo = $imagem['type'];
        
        if(in_array($tipo, $permite)){
            $funcao->uploads($destino, $nome, 620, $pasta, $tipo);
            $objeto->set('imagem', $nome);
        }else{
            echo "Imagem inválida";
        }
      }
    
    extract($_POST);

    if($nome == ''){
        $msg="Digite seu Nome!";
    }else if($email==''){
        $msg="Digite seu E-mail!";
    }elseif(!preg_match("/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/i",$email)){
        $msg="E-mail Invalido";
    }  else if($titulo==''){
        $msg="Digite o Titulo do Post!";
    }  elseif ($catg=='') {
        $msg="Digite a Categoria do Post!";
    }else{

    $objeto->set('nome', $nome);
    $objeto->set('email', $email);
    $objeto->set('categoria', $catg);
    $objeto->set('titulo', $titulo);
    $objeto->set('texto', $texto);
    $objeto->set('data', $funcao->data());
    $objeto->set('video', $video);
    
        if($objeto->incluir()){
            $msg = "Enviado com Sucesso";
            $nome="";
            $email="";
            $catg="";
            $titulo="";
            $video="";
        }
    }
}

?>
<?php include_once 'includes/direita.php'; ?>

<div id="contentPost">
    <div id="blocoPost">
        <h1>Envie seu post:</h1><br/>
        <?php if($msg!=""){ echo "<p name='msg'>$msg</p><br/>";}?>
       <form method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="envio" value="enviodepost" />
            <table border="0" cellspacing="10">
                <tr><td><h3 style="color: black;">Nome:</h3></td><td><input type="text" name="nome" value="<?php echo $nome;?>"/></td></tr>
                <tr><td><h3 style="color: black;">E-mail:</h3></td><td><input type="text" name="email" value="<?php echo $email;?>"/></td></tr>
                <tr><td><h3 style="color: black;">Titulo:</h3></td><td><input type="text" name="titulo" value="<?php echo $titulo;?>"/></td></tr>
                <tr><td><h3 style="color: black;">Categoria:</h3></td><td><input type="text" name="catg" value="<?php echo $catg;?>"/></td></tr>
                <tr><td><h3 style="color: black;">Imagem:</h3></td><td><input type="file" name="imagem" /></td></tr>
                <tr><td><h3 style="color: black;">Video:</h3></td><td><input type="text" name="video" /></td></tr>
                <tr><td valign="top"><h3 style="color: black;">Texto:</h3></td><td><textarea cols="10" rows="19" name="texto"></textarea></td></tr>
                <tr><td colspan="2" name="botao" align="right"><input type="submit" value="Enviar"/> </td></tr>
            </table>
        </form>


        <h3 align="left">Observações:</h3>
        <p name="obs">*O post enviado será submetido a uma avaliação de conteudo, para possiveis correções ortograficas e checagem de conteudo.</p>
        <p name="obs">*Assim que passar pela avaliação será postado no site e seu criador será devidamente mensionado como o autor do post.</p>
        <p name="obs">*Em caso de problemas, lhe enviaremos um email informando-o dos impeclios para a não publicação no blog.</p>
    </div>
</div>

<?php include_once 'includes/footer.php';?>